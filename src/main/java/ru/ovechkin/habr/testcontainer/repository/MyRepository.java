package ru.ovechkin.habr.testcontainer.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import ru.ovechkin.habr.testcontainer.entity.MyEntity;

public interface MyRepository extends JpaRepository<MyEntity, Long> {
}
