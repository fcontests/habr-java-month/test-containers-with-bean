package ru.ovechkin.habr.testcontainer.annotation;

import ru.ovechkin.habr.testcontainer.container.PostgresContainer;

import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

@Retention(RetentionPolicy.RUNTIME)
@Target(ElementType.TYPE)
public @interface AutoConfigurePostgresContainer {

    /**
     * Название бина, в BeanDefinition которому пропишется зависимость на #{@link PostgresContainer}
     */
    String beanNameThatNeedsToDependOnContainer() default "";

    /**
     * Класс бина, в BeanDefinition которому пропишется зависимость на #{@link PostgresContainer}
     */
    Class<?> beanClassNameThatNeedsToDependOnContainer() default Void.class;

}