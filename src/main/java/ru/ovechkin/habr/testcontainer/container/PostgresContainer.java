package ru.ovechkin.habr.testcontainer.container;

import lombok.Getter;
import org.testcontainers.containers.PostgreSQLContainer;
import org.testcontainers.utility.DockerImageName;

import javax.annotation.PostConstruct;
import javax.annotation.PreDestroy;

@Getter
public class PostgresContainer {

    private final PostgreSQLContainer<?> postgreSQLContainer;

    public PostgresContainer() {
        postgreSQLContainer = new PostgreSQLContainer<>(DockerImageName.parse("postgres:14.3"))
                .withDatabaseName("postgres")
                .withUsername("postgres")
                .withPassword("example")
                .withExposedPorts(5432)
        ;
    }

    @PostConstruct
    public void start() {
        postgreSQLContainer.start();

        System.setProperty("spring.datasource.url", postgreSQLContainer.getJdbcUrl());
        System.setProperty("spring.datasource.username", postgreSQLContainer.getUsername());
        System.setProperty("spring.datasource.password", postgreSQLContainer.getPassword());
    }

    @PreDestroy
    public void stop() {
        postgreSQLContainer.stop();
    }

}