package ru.ovechkin.habr.testcontainer.context.customizer;

import org.springframework.context.ConfigurableApplicationContext;
import org.springframework.test.context.ContextCustomizer;
import org.springframework.test.context.MergedContextConfiguration;
import ru.ovechkin.habr.testcontainer.annotation.AutoConfigurePostgresContainer;

public class ContainerContextCustomizer implements ContextCustomizer {

    private final AutoConfigurePostgresContainer annotation;
    public ContainerContextCustomizer(AutoConfigurePostgresContainer annotation) {
        this.annotation = annotation;
    }

    @Override
    public void customizeContext(ConfigurableApplicationContext context, MergedContextConfiguration mergedConfig) {
        context.addBeanFactoryPostProcessor(new DependsOnContainerSetterBFPP(annotation));
    }

}
