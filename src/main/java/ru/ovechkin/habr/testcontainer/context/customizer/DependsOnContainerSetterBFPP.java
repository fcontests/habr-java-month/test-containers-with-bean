package ru.ovechkin.habr.testcontainer.context.customizer;

import org.springframework.beans.BeansException;
import org.springframework.beans.factory.config.BeanFactoryPostProcessor;
import org.springframework.beans.factory.config.ConfigurableListableBeanFactory;
import org.springframework.beans.factory.support.BeanDefinitionBuilder;
import org.springframework.beans.factory.support.BeanDefinitionRegistry;
import org.springframework.core.Ordered;
import ru.ovechkin.habr.testcontainer.annotation.AutoConfigurePostgresContainer;
import ru.ovechkin.habr.testcontainer.container.PostgresContainer;

import static java.beans.Introspector.decapitalize;

public class DependsOnContainerSetterBFPP implements BeanFactoryPostProcessor, Ordered {

    private final AutoConfigurePostgresContainer annotation;

    public DependsOnContainerSetterBFPP(AutoConfigurePostgresContainer annotation) {
        this.annotation = annotation;
    }

    @Override
    public void postProcessBeanFactory(ConfigurableListableBeanFactory beanFactory) throws BeansException {
        registerContainer(beanFactory);
        setDependsOn(beanFactory, annotation);
    }

    /**
     * Регистрируется класс #{@link PostgresContainer}
     */
    private void registerContainer(ConfigurableListableBeanFactory beanFactory) {
        BeanDefinitionRegistry registry = (BeanDefinitionRegistry) beanFactory;
        registry.registerBeanDefinition(decapitalize(PostgresContainer.class.getSimpleName()),
                BeanDefinitionBuilder.genericBeanDefinition(PostgresContainer.class).getBeanDefinition());
    }

    /**
     * Заставляем класс {@code annotation.beanToSubscribeOnEnvironmentSetter}
     * зависеть от {@link PostgresContainer}
     */
    private void setDependsOn(ConfigurableListableBeanFactory beanFactory, AutoConfigurePostgresContainer annotation) {
        beanFactory.getBeanDefinition(geBeanWhoNeedsToDepend(annotation))
                .setDependsOn(decapitalize(PostgresContainer.class.getSimpleName()));
    }

    /**
     * @return название бина, который надо подписать на {@link PostgresContainer}
     * Название бина достаётся либо из {@param annotation.beanNameThatNeedsToDependOnContainer}
     * либо из класса {@param annotation.beanClassNameThatNeedsToDependOnContainer}
     * название которого переводится в стрингу и понижается первая буква
     */
    private String geBeanWhoNeedsToDepend(AutoConfigurePostgresContainer annotation) {
        String beanToSubscribe = annotation.beanNameThatNeedsToDependOnContainer();
        if (!beanToSubscribe.isEmpty()) {
            return beanToSubscribe;
        }
        Class<?> aClass = annotation.beanClassNameThatNeedsToDependOnContainer();
        if (aClass != Void.class) {
            return decapitalize(aClass.getSimpleName());
        }
        throw new RuntimeException();
    }

    /**
     * Порядок выставляем самый приоритетный, чтобы зависимость прописалась раньше всего
     */
    @Override
    public int getOrder() {
        return 0;
    }
}
