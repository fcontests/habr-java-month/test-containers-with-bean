package ru.ovechkin.habr.testcontainer.context.customizer;

import org.springframework.test.context.ContextConfigurationAttributes;
import org.springframework.test.context.ContextCustomizer;
import org.springframework.test.context.ContextCustomizerFactory;
import ru.ovechkin.habr.testcontainer.annotation.AutoConfigurePostgresContainer;

import java.util.List;

public class ContainerContextCustomizerFactory implements ContextCustomizerFactory {

    @Override
    public ContextCustomizer createContextCustomizer(Class<?> testClass, List<ContextConfigurationAttributes> configAttributes) {
        AutoConfigurePostgresContainer annotation = testClass.getAnnotation(AutoConfigurePostgresContainer.class);

        return new ContainerContextCustomizer(annotation);
    }
}