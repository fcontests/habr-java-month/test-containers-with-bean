package ru.ovechkin.habr.testcontainer;

import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import ru.ovechkin.habr.testcontainer.annotation.AutoConfigurePostgresContainer;
import ru.ovechkin.habr.testcontainer.container.PostgresContainer;
import ru.ovechkin.habr.testcontainer.entity.MyEntity;
import ru.ovechkin.habr.testcontainer.repository.MyRepository;

import javax.sql.DataSource;

import java.util.List;

import static org.assertj.core.api.Assertions.assertThat;
import static org.junit.jupiter.api.Assertions.assertTrue;

@SpringBootTest
@AutoConfigurePostgresContainer(beanClassNameThatNeedsToDependOnContainer = DataSource.class)
class DatabaseTest {

    @Autowired
    @SuppressWarnings("SpringJavaInjectionPointsAutowiringInspection")// спринг не понимает что этот контейнер будет зарегестрирован как бин
    private PostgresContainer postgresContainer;

    @Autowired
    private MyRepository repository;

    @Test
    void testConnect() {
        assertTrue(postgresContainer.getPostgreSQLContainer().isRunning());
    }

    @Test
    void testRepo() {
        MyEntity testEntity = new MyEntity().setName("testName");
        repository.save(testEntity);

        List<MyEntity> all = repository.findAll();
        assertThat(testEntity).isEqualTo(all.get(0));
    }

}